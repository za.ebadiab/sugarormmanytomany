package com.example.sugarormmanytomany.model;

import com.orm.SugarRecord;

import java.util.List;

public class Author extends SugarRecord {

    String name;

    public Author() {
    }

    public Author(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<AuthorBookManyToMany> getAuthorBookManyToMany() {
        return AuthorBookManyToMany.find(AuthorBookManyToMany.class, "author = ?", String.valueOf(this.getId()));
    }


}
