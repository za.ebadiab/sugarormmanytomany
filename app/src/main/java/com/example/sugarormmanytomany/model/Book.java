package com.example.sugarormmanytomany.model;

import com.orm.SugarRecord;

public class Book extends SugarRecord {
    String name;

    public Book() {
    }

    public Book(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
