package com.example.sugarormmanytomany.model;

import com.orm.SugarRecord;

public class AuthorBookManyToMany extends SugarRecord {
    public Author author;
    public Book book;

    public AuthorBookManyToMany() {
    }

    public AuthorBookManyToMany(Author author, Book book) {
        this.author = author;
        this.book = book;
    }
}
