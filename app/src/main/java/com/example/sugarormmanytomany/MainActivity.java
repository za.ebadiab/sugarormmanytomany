package com.example.sugarormmanytomany;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

import com.example.sugarormmanytomany.model.Author;
import com.example.sugarormmanytomany.model.AuthorBookManyToMany;
import com.example.sugarormmanytomany.model.Book;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//  reference: https://github.com/chennaione/sugar/issues/60
//        http://satyan.github.io/sugar/creation.html

        Author author1 = new Author("author1");
        author1.save();
        Author author2 = new Author("author2");
        author2.save();

        Book book1 = new Book("book1");
        book1.save();
        Book book2 = new Book("book2");
        book2.save();
        Book book3 = new Book("book3");
        book3.save();


        AuthorBookManyToMany authorBookManyToMany1 = new AuthorBookManyToMany(author1, book1);
        authorBookManyToMany1.save();
        AuthorBookManyToMany authorBookManyToMany2 = new AuthorBookManyToMany(author1, book2);
        authorBookManyToMany2.save();
        AuthorBookManyToMany authorBookManyToMany3 = new AuthorBookManyToMany(author1, book3);
        authorBookManyToMany3.save();

        AuthorBookManyToMany authorBookManyToMany4 = new AuthorBookManyToMany(author2, book1);
        authorBookManyToMany4.save();
        AuthorBookManyToMany authorBookManyToMany5 = new AuthorBookManyToMany(author2, book3);
        authorBookManyToMany5.save();


        List<AuthorBookManyToMany> authorBookManyToManyList = AuthorBookManyToMany.listAll(AuthorBookManyToMany.class);
        Log.d("TAG", "authorBookManyToManyList.size()=  " + authorBookManyToManyList.size() + " author.size= " + Author.count(Author.class) + " book.size= " + Book.count(Book.class));


        // search not to be repetetive book
        Author author3 = new Author("author3");
        author3.save();


        for (int i = 0; i < authorBookManyToManyList.size(); i++) {
            Log.d("TAG", "author.getName(): " + authorBookManyToManyList.get(i).author.getName() + "  book.getName()= " + authorBookManyToManyList.get(i).book.getName());
        }

        Book book = Book.find(Book.class, "name= ?", "book2").get(0);
        if (book != null) {
            AuthorBookManyToMany authorBookManyToMany6 = new AuthorBookManyToMany(author3, book);
            authorBookManyToMany6.save();

        } else {

            AuthorBookManyToMany authorBookManyToMany6 = new AuthorBookManyToMany(author3, new Book("book5"));
            authorBookManyToMany6.save();
        }
        authorBookManyToManyList = AuthorBookManyToMany.listAll(AuthorBookManyToMany.class);
        Log.d("TAG", "authorBookManyToManyList.size()=  " + authorBookManyToManyList.size() + " author.size= " + Author.count(Author.class) + " book.size= " + Book.count(Book.class));

        for (int i = 0; i < authorBookManyToManyList.size(); i++) {
            Log.d("TAG", "author.getName(): " + authorBookManyToManyList.get(i).author.getName() + "  book.getName()= " + authorBookManyToManyList.get(i).book.getName());
        }
    }
}